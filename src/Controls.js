import React from 'react';
import ReactDOM from 'react-dom';
import Video from './Video';


var Controls = React.createClass({
  getInitialState: function() {
    return {
      start: true,
      localFavorite: false
    };
  },
  controlOnClick: function(event) {
      this.props.someFunc(event);

      var attribute = event.target.getAttribute('data-elem');
      if (attribute == 'play') {
        this.setState({start:!this.state.start});
      }
      if (attribute == 'pause') {
        this.setState({start: !this.state.start});
      }
      if (attribute == 'backward') {
       this.setState({start: true});
      }
      if (attribute == 'forward') {
       this.setState({start: true});
      }
  },
  addToFavorite: function() {
    if (this.props.favorite == false) {
      this.props.addToFavorite();  
    } else {
      this.props.deleteFromFavorite();  
    }
    // this.setState({localFavorite: !this.state.localFavorite});
  },
  render: function() {
    var start = this.state.start;
    var localFavorite = this.state.localFavorite;
    var favorite = this.props.favorite;
    return (
        <div className="controls"> 
          <div className="controls__elem controls__elem_backward js-nav-item" 
               data-elem="backward" 
               tabIndex='1'
               onClick={this.controlOnClick}></div>
          <div className={"controls__elem js-nav-item " + (start ? "controls__elem_play" : "controls__elem_pause")} 
               data-elem={(start ? "play" : "pause")} 
               tabIndex='1'
               onClick={this.controlOnClick}></div>
          <div className="controls__elem controls__elem_forward js-nav-item" 
               tabIndex='1'
               data-elem="forward" 
               onClick={this.controlOnClick}></div>
          <div className={"controls__elem js-nav-item " + ( favorite ? "controls__elem_favorite" : "controls__elem_nofavorite")} 
               tabIndex='1'
               data-elem="favorite" 
               ></div>
        </div>
      )
  }
});
export default Controls;