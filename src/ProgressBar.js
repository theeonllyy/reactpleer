import React from 'react';
import ReactDOM from 'react-dom';

var ProgressBar = React.createClass({
  render: function() {
    var playedStyle = {width: this.props.percentPlayed + '%'}
    var bufferStyle = {width: this.props.percentBuffered + '%'}
    return (
      <div className='progress-bar progress_bar_ref'  onClick={this.props.handleProgressClick}>
        <div className="progress-bar__buffered" style={bufferStyle}></div>
        <div className='progress-bar__elem' style={playedStyle}></div>
      </div>
    )
  }
});

export default ProgressBar;