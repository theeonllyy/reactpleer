import React from 'react';
import ReactDOM from 'react-dom';

var my_sort = [
  {
    name: 'Комедия'
  },
  {
    name: 'Драма'
  },
  {
    name: 'Спорт'
  },
  {
    name: 'Фантастика'
  },
  {
    name: 'Ужасы'
  }
]

var Sort = React.createClass({
  getInitialState: function() {
    return {
      sort: my_sort,
      sortParametr: 'all'
    };
  },
  sortFilms: function(event) {
    var target = event.target.getAttribute('data-value');
    this.setState({sortParametr: target});
  },
  updateVideo: function(event) {
    var target = event.target.getAttribute('data-videoid');
    this.props.updateVideo(target);
  },
  render: function() {
    var data = this.state.sort;
    var dataFilms = this.props.data;
    var sortTemplate, sortFilmTemplate = '';
    var sortParametr = this.state.sortParametr;
    var self = this;

    sortTemplate =  data.map(function(item, index) {
                      return (
                        <div data-value={item.name} 
                             key={index} 
                             onClick={self.sortFilms} 
                             className={'sort__item js-nav-item ' + (sortParametr == item.name ? 'sort__item_active' : '')}>
                          {item.name}
                        </div>
                      )
                    });
    sortFilmTemplate = dataFilms.map(function(item,index) {
                        return (
                            <div key={index} 
                                 data-sort={item.genre} 
                                 data-videoid={item.videoId}
                                 onClick={self.updateVideo}
                                 className={'sort__film-name js-nav-item ' + (sortParametr == item.genre || sortParametr == 'all' ? 'sort__film-name_active' : '')}>
                              {item.name}
                            </div>
                          );
                       });
    return (
      <div className='sort'>
        <div className='sort__values'>
          {sortTemplate}
          <div data-value='all' 
               onClick={this.sortFilms} 
               className={'sort__item js-nav-item ' + (sortParametr == 'all' ? 'sort__item_active' : '')}>
               Все</div>
        </div>
        <div className='sort__films'>
          {sortFilmTemplate}
        </div>
      </div>
    )
  }
});

export default Sort;