import React from 'react';
import ReactDOM from 'react-dom';


var Time = React.createClass({
  render: function() {
    var currentTime = this.props.currentTime || 0;
    var duration = this.props.duration;
    var videoName = this.props.videoName;
    var untilEnd = parseInt(duration-currentTime)+' cек';
    duration = parseInt(duration)+ ' ceк'
    return (
      <div className='time'>
          <div className='time__info-left'>
            <span>Сейчас: </span> 
            <span className='time__film-name'>{videoName}</span>
            <div className='time__fulltime'>{duration}</div>
          </div>
          <div className='time__info-right'>
            Окончание через {untilEnd} 
          </div>

      </div>
    )
  }
});

export default Time;