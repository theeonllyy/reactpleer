import React from 'react';
import ReactDOM from 'react-dom';
import Video from './Video';
var my_video = [
  {
    videoId : 0,
    src : 'http://u42697.netangels.ru/video/donjuan.mp4',
    name : 'Страсти по Дон-Жуану',
    genre : 'Комедия',
    poster : 'http://movieweb.ru/media/still/don-jon-hd/don-jon-hd_9b.jpg'
  },
  {
    videoId : 1,
    src : 'http://u42697.netangels.ru/video/forrestgump.mp4',
    name : 'Форест гамп',
    genre : 'Драма',
    poster : 'http://cdn.fishki.net/upload/post/201506/12/1564157/001.jpg'
  },
  {
    videoId : 2,
    src : 'http://u42697.netangels.ru/video/forsazh-7-rutrailer2_640.mp4',
    name : 'Форсаж 7',
    genre : 'Спорт',
    poster : 'https://i.ytimg.com/vi/bNTG9eBrTmY/maxresdefault.jpg'
  },
  {
    videoId : 3,
    src : 'http://u42697.netangels.ru/video/x-men.mp4',
    name : 'Люди Икс',
    genre : 'Фантастика',
    poster : 'http://api.vkino.com.ua/posters/97/97f8a5d206c5b90505e89345842b45321bfd7bea.C1920x1080.jpg'
  },
  {
    videoId : 4,
    src : 'http://u42697.netangels.ru/video/hangover.mov',
    name : 'Мальчишник в вегасе',
    genre : 'Комедия',
    poster : 'http://kinodom.org/uploads/posts/2014-01/1390295567_2342362.jpg'
  },
  {
    videoId : 5,
    src : 'http://u42697.netangels.ru/video/iron_man.mov',
    name : 'Железный человек',
    genre : 'Фантастика',
    poster : 'http://getbg.net/upload/full/394989_iron-man_zheleznyj-chelovek_robert-dauni_1920x1080_(www.GetBg.net).jpg'
  },
  {
    videoId : 6,
    src : 'http://u42697.netangels.ru/video/mertvec.mp4',
    name : 'Какие-то там мертвецы',
    genre : 'Комедия',
    poster : 'http://oboibox.ru/orig/Zhivye-mertvecy(oboibox.ru).jpg'
  },
  {
    videoId : 7,
    src : 'http://u42697.netangels.ru/video/nevesta.mp4',
    name : 'Невеста',
    genre : 'Ужасы',
    poster : 'http://worldzet.ru/wp-content/uploads/2016/07/the-cure-2016.jpg'
  },
  {
    videoId : 8,
    src : 'http://u42697.netangels.ru/video/obitelzla.mp4',
    name : 'Обитель зла',
    genre : 'Фантастика',
    poster : 'http://wpapers.ru/wallpapers/films/10431/1920x1080_%D0%9E%D0%B1%D0%B8%D1%82%D0%B5%D0%BB%D1%8C-%D0%B7%D0%BB%D0%B0.jpg'
  },
  {
    videoId : 9,
    src : 'http://u42697.netangels.ru/video/rosomaha.mp4',
    name : 'Росомаха',
    genre : 'Ужасы',
    poster : 'http://wpapers.ru/wallpapers/films/17039/1920x1080_%D0%A0%D0%BE%D1%81%D0%BE%D0%BC%D0%B0%D1%85%D0%B0-%D0%91%D0%B5%D1%81%D1%81%D0%BC%D0%B5%D1%80%D1%82%D0%BD%D1%8B%D0%B9.jpg'
  },
  {
    videoId: 10,
    src : 'http://u42697.netangels.ru/video/x-men_apocalypse.mp4',
    name : 'Люди икс апокалипсис',
    genre : 'Комедия',
    poster : 'http://filmask.ru/uploads/posts/1374067679_lyudi-iks-dni-minuvshego-buduschego-2014.jpg'
  }
];

var App = React.createClass({
  getInitialState: function() {
    return {
      video: my_video
    };
  },
  componentDidMount: function() {
    var self = this;
  },
  componentWillUnmount: function() {
  },
  render: function() {
    console.log('render');
    return (
      <div className='app'>
        <Video data={this.state.video} />
      </div>
    );
  }
});
export default App;