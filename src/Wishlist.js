import React from 'react';
import ReactDOM from 'react-dom';



var Wishlist = React.createClass({
  render: function() {
    var videoInFavor = this.props.videoInFavor;
    var wishlistTemplate = videoInFavor.map(function(item, index) {
                                return (
                                  <div data-value={item.name} 
                                       key={index}  
                                       className={'sort__film-name sort__film-name_active js-nav-item'}>
                                    {item.name}
                                  </div>
                                )
                              });
    return (
      <div className='wishlist'>
        {wishlistTemplate}
      </div>
    )
  }
});

export default Wishlist;