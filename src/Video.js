import React from 'react';
import ReactDOM from 'react-dom';
import Controls from './Controls';
import Time from './Time';
import ProgressBar from './ProgressBar';
import Sort from './Sort';
import Wishlist from './Wishlist';

var Video = React.createClass({
  propTypes: {
    data: React.PropTypes.array.isRequired
  },
  getInitialState() {
     return {
         videoId: 0,
         percentPlayed: 0,
         duration: 0,
         percentBuffered: 0,
         videoInFavor: [],
         favorite: false
     };
  },
  handleChildClick: function(event) {
    var attribute = event.target.getAttribute('data-elem');
    var object = ReactDOM.findDOMNode(this.refs.video);
    var self = this;
    if (attribute == 'play') {
      object.play();
    }
    if (attribute == 'pause') {
      object.pause();
    }
    if (attribute == 'backward') {
      this.setState({percentPlayed: 0});
      this.setState({duration: 0});
      this.setState({percentBuffered: 0});
      this.setState({currentTime: 0});
      this.setState({videoId: --this.state.videoId}, object.load());
      if (this.state.videoId < 0) {
        this.setState({videoId : (this.props.data.length-1)}, object.load())
      }
      setTimeout(function(){
        self.isFavorite();
      },0);
    }
    if (attribute == 'forward') {
      this.setState({percentPlayed: 0});
      this.setState({duration: 0});
      this.setState({percentBuffered: 0});
      this.setState({currentTime: 0});
      this.setState({videoId: ++this.state.videoId});
      if (this.state.videoId == (this.props.data.length) ) {
        this.setState({videoId : 0}, object.load())
      }
      setTimeout(function(){
        self.isFavorite();
      },0);      
    }
  },
  isFavorite: function(){
    var favorArray = this.state.videoInFavor;
    var data = this.props.data;
    console.log(this.state.videoId);
    if (favorArray.indexOf(data[this.state.videoId]) == (-1)) {
      this.setState({favorite: false});
      console.log('state: '+this.state.favorite);
    } else {
      this.setState({favorite: true});
      console.log('state: '+this.state.favorite);
    }
  },
  updateBufferBar: function(buffered){
    this.setState({percentBuffered: buffered});
  },
  updateDuration: function(duration){
    this.setState({duration: duration});
  },
  updateProgressBar: function(times){
    var percentPlayed = Math.floor((100 / times.duration) * times.currentTime);

    this.setState({
      currentTime: times.currentTime,
      percentPlayed: percentPlayed,
      duration: times.duration
    });
  },
  componentDidMount: function(){
    var video = ReactDOM.findDOMNode(this.refs.video);
    var self = this;

    var bufferCheck = setInterval(function(){
      try{
          var percent = (video.buffered.end(0) / video.duration * 100)
      } catch(ex){
        percent = 0;
      }
      self.updateBufferBar(percent);


    }, 500);

    video.addEventListener('durationchange', function(e){
      self.updateDuration(e.target.duration);
    }, false);

    video.addEventListener('timeupdate', function(e){
      self.updateProgressBar({
        currentTime: e.target.currentTime,
        duration: e.target.duration
      });
    }, false)
  },
  sortUpdateVideo: function(target) {
    var object = ReactDOM.findDOMNode(this.refs.video);
    this.setState({percentPlayed: 0});
    this.setState({duration: 0});
    this.setState({percentBuffered: 0});
    this.setState({currentTime: 0});
    this.setState({videoId: target}, object.load());
  },
  addToFavorite: function() {
    var data = this.props.data;
    var favorArray = this.state.videoInFavor;
    favorArray.push(data[this.state.videoId]);
    this.setState({videoInFavor: favorArray});
  },
  deleteFromFavorite: function() {
    var data = this.props.data;
    var favorArray = this.state.videoInFavor;
    console.log(this.state.videoId);
    //favorArray.splice(data[this.state.videoId], 1);
    console.log('test');
    console.log('cons' + favorArray.indexOf(data[this.state.videoId]));
    this.setState({videoInFavor: favorArray});
    // console.log(favorArray);
  },
  render: function() {
    var data = this.props.data;
    var videoTemplate;

    if (data.length > 0) {
      videoTemplate = <div className='video__scene' >
                        <video  
                            src={data[this.state.videoId].src}  
                            className='video__elem' 
                            poster={data[this.state.videoId].poster}
                            ref="video">
                        </video>
                        <Wishlist data={data}
                                  videoInFavor={this.state.videoInFavor}/>
                        <Sort data={data}
                              updateVideo={this.sortUpdateVideo}/>
                        <Time 
                            currentTime={this.state.currentTime} 
                            duration={this.state.duration}
                            videoName={data[this.state.videoId].name}
                            />
                        <ProgressBar 
                            percentPlayed={this.state.percentPlayed} 
                            percentBuffered={this.state.percentBuffered} />
                        <Controls someFunc={this.handleChildClick}
                                  addToFavorite={this.addToFavorite}
                                  favorite={this.state.favorite}
                                  deleteFromFavorite={this.deleteFromFavorite} />
                      </div>
    } else {
      videoTemplate = <p>К сожалению видео нет</p>
    }

    return (
      <div className='video'>
        {videoTemplate}
      </div>
    );
  }
});

export default Video;